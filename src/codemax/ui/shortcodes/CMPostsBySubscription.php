<?php
namespace codemax\ui\shortcodes;

abstract class CMPostsBySubscription
{
    
    /**
     * El shortcode muestra un grilla de publicaciones filtradas por nivel de suscripción. 
     */
    static public function install(): void
    {
        add_shortcode( 'cm_posts_by_subscription', [ CMPostsBySubscription::class, 'code' ] );
    }
    
    /**
     * 
     * @param array $atts
     * @return string
     */
    static public function code(): string
    {
        // Filtro de miembros con la suscripción nivel Gratis
        $code = do_shortcode( '[the-post-grid id="4501" title=""]' );
        
        if ( is_user_logged_in() )
        {
            $user_id = get_current_user_id();
            if ( function_exists( 'pms_is_member' ) )
            {
                if ( pms_is_member( $user_id, [ 4478 ] ) )
                { // Filtro de miembros con la suscripción premiun
                    $code = do_shortcode( '[the-post-grid id="4503" title=""]' );
                }
                elseif ( pms_is_member( $user_id, [ 4477 ] ) )
                { // Filtro de miembros con la suscripción base
                    $code = do_shortcode( '[the-post-grid id="4502" title=""]' );
                }
            }
        }
        return $code;
    }
}

