<?php
/*
 Plugin name: CDC Plugin fix.
 Description: WP plugin fix para el sitio web https://desarrollocreativo.com.ar/.
 Author: Codemax
 Author URI: https://codemax.com.ar
 Version: 1.0.10.19
 */

define( CDC_FULL_DIR, __DIR__. DIRECTORY_SEPARATOR );

require_once CDC_FULL_DIR.'vendor/autoload.php';

function cdc_add_script()
{
    wp_enqueue_style( 'cdc-fix-css', plugins_url( 'app/ui/css/cdc-fix.css', __FILE__ ), [], '1.0.10.19' );
}

add_action( 'wp_enqueue_scripts', 'cdc_add_script', 5 );


/* Extension que muestra un grilla de publicaciones filtradas por nivel de suscripción  */
codemax\ui\shortcodes\CMPostsBySubscription::install();
